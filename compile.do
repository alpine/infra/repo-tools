exec >&2

redo-ifchange config.redo

command="go build"

. ./config.redo

if [ -n "$LDFLAGS" ]; then
    command="$command -ldflags '$LDFLAGS'"
fi

if [ -n "$GCFLAGS" ]; then
    command="$command -gcflags='$GCFLAGS'"
fi

if [ -n "$TAGS" ]; then
    command="$command -tags $TAGS"
fi

command="$command -o \"\$2\" \"\$1\""

cat <<EOF >$3
#!/bin/sh
$command
EOF
chmod +x $3
