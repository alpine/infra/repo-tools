package utils

func ListContains(list []string, needle string) bool {
	for _, elem := range list {
		if elem == needle {
			return true
		}
	}

	return false
}
