package utils

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func Confirm(msg string) (bool, error) {
	buf := bufio.NewReader(os.Stdin)

	for {
		fmt.Printf("%s? [y/n]: ", msg)
		resp, err := buf.ReadString('\n')
		if err != nil {
			return false, err
		}

		switch strings.ToLower(strings.TrimSpace(resp)) {
		case "y", "yes":
			return true, nil
		case "n", "no":
			return false, nil
		default:
			fmt.Println("Please provide a valid option, cltr+c to abort")
		}
	}
}

func Fatalf(msg string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, msg, args...)
	os.Exit(1)
}
