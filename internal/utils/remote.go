package utils

import (
	"fmt"
	"io"
	"mime"
	"net/http"
	"path"
)

type HttpFile struct {
	Stream        io.ReadCloser
	RemoteName    string
	ContentType   string
	ContentLength int64
}

func FetchFile(url string) (file HttpFile, err error) {
	resp, err := http.Get(url)
	if err != nil {
		return
	}

	if resp.StatusCode != 200 {
		err = fmt.Errorf("Received unexpected status for %s: %d", url, resp.StatusCode)
	}

	file = HttpFile{
		Stream:        resp.Body,
		ContentType:   resp.Request.Header.Get("Content-Type"),
		ContentLength: resp.ContentLength,
		RemoteName:    getResponseFilename(resp),
	}

	return
}

func getResponseFilename(resp *http.Response) (filename string) {
	contentDisposition := resp.Header.Get("Content-Disposition")

	if contentDisposition != "" {
		if _, params, err := mime.ParseMediaType(contentDisposition); err != nil {
			filename = params["filename"]
		}
	}

	if filename == "" {
		filename = path.Base(resp.Request.RequestURI)
	}

	return
}
