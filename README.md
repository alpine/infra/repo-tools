# repo-tools

Set of tools in a single binary to interact with Alpine Linux repositories.

## Building

Dependencies:

* go
* redo
* scdoc

``` sh
./configure --prefix /usr --strip
redo
DESTDIR=$destdir redo install
```

## Quick Usage

``` sh
repo-tools fastly purge pkg --origin=vim --release=v3.15
```

