exec >&2

redo-ifchange compile
find . -name '*.go' -exec redo-ifchange {} +

./compile gitlab.alpinelinux.org/alpine/infra/repo-tools "$3"

#go build -tags purgeall -o ./$3 ./$2.go
