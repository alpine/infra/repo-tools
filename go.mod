module gitlab.alpinelinux.org/alpine/infra/repo-tools

go 1.23

require (
	github.com/spf13/cobra v1.8.1
	gitlab.alpinelinux.org/alpine/go v0.10.1
)

require (
	github.com/MakeNowJust/heredoc/v2 v2.0.1 // indirect
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	gopkg.in/ini.v1 v1.67.0 // indirect
)

// replace gitlab.alpinelinux.org/alpine/go => ../../alpine-go
