exec >&2

case $1 in
    *.?);;
    *) echo "Invalid target $1"; exit 1;;
esac

redo-ifchange $1.scd

scdoc <$1.scd >$3
