package main

import (
	"gitlab.alpinelinux.org/alpine/infra/repo-tools/cmd"
	_ "gitlab.alpinelinux.org/alpine/infra/repo-tools/cmd/fastly"
)

func main() {
	cmd.Execute()
}
