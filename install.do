exec >&2

: ${DESTDIR=}

redo-ifchange all

. ./config.redo

install -Dm0755 repo-tools "$DESTDIR$PREFIX/bin/repo-tools"

install -Dm0644 completion/zsh.compl "$DESTDIR$PREFIX/share/zsh/site-functions/_repo-tools"
install -Dm0644 completion/fish.compl "$DESTDIR$PREFIX/share/fish/completions/repo-tools.fish"
install -Dm0644 completion/bash.compl "$DESTDIR$PREFIX/share/bash-completion/completions/repo-tools"

for f in doc/*.[0-9]; do
    manpage=${f#*/}
    section=${manpage#*.}
    install -Dm0644 "$f" "$DESTDIR$PREFIX"/share/man/man$section/"$manpage"
done
