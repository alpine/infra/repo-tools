//go:build purgeall

package fastly

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.alpinelinux.org/alpine/go/releases"
	"gitlab.alpinelinux.org/alpine/go/repository"
	"gitlab.alpinelinux.org/alpine/infra/repo-tools/internal/utils"
)

type AllArgs struct {
	releaseName string
}

var (
	allArgs PkgArgs
)

func init() {
	purgeAllCmd := &cobra.Command{
		Use:   "all",
		Short: "Purge all packages for a specific release",
		Run:   fastlyPurgeAllCmd,
	}

	purgeAllCmd.Flags().StringVar(&allArgs.releaseName, "release", "latest-stable", "Which release to purge")

	purgeCmd.AddCommand(purgeAllCmd)

}

func fastlyPurgeAllCmd(cmd *cobra.Command, arg []string) {
	confirmed, err := utils.Confirm("Are you sure you want to purge the fastly CDN cache")
	if err != nil {
		log.Fatal(err)
	}

	if !confirmed {
		return
	}

	alpineReleases, err := releases.Fetch()

	if err != nil {
		log.Fatalf("Fetching releases: %s", err)
	}

	log.Print("Fetching indexes")

	repositories := map[string]*repository.RepositoryWithIndex{}
	for _, arch := range alpineReleases.Architectures {
		if arch == "mips64" {
			continue
		}

		for _, repoName := range []string{"main", "community"} {
			repo := repository.NewRepositoryFromComponents(mirror, allArgs.releaseName, repoName, arch)

			indexFile, err := utils.FetchFile(repo.IndexUri())

			if err != nil {
				log.Printf("ERROR: fetching index for %s: %s", arch, err)
				continue
			}

			index, err := repository.IndexFromArchive(indexFile.Stream)

			if err != nil {
				log.Printf("ERROR: parsing index for %s: %s", arch, err)
				continue
			}

			repositories[arch+"/"+repoName] = repo.WithIndex(index)
		}
	}

	for id, repo := range repositories {
		log.Printf("Purging packages for: %s", id)
		pkgs := repo.Packages()

		Purge(pkgs, 4)
	}
}
