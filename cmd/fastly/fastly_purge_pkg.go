package fastly

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.alpinelinux.org/alpine/go/releases"
	"gitlab.alpinelinux.org/alpine/go/repository"
	"gitlab.alpinelinux.org/alpine/infra/repo-tools/internal/utils"
)

type PkgArgs struct {
	origin   bool
	filename bool

	dryRun bool

	releaseName    string
	repositoryName string
	arch           string
}

var (
	pkgArgs PkgArgs
)

func init() {
	purgePkgCmd := &cobra.Command{
		Use:   "pkg [--origin|--filename] <pkg..>",
		Short: "Purge specific packages",
		Args:  cobra.MinimumNArgs(1),
		Run:   fastlyPurgePkgCmd,
	}

	purgePkgCmd.Flags().BoolVar(&pkgArgs.origin, "origin", false, "Purge packages by origin instead of package name")
	purgePkgCmd.Flags().BoolVar(&pkgArgs.filename, "filename", false, "Purge packages by package filename instead of package name")

	purgePkgCmd.Flags().BoolVarP(&pkgArgs.dryRun, "dryrun", "n", false, "Do not actually purge, only show what would be purged")

	purgePkgCmd.Flags().StringVar(&pkgArgs.releaseName, "release", "edge", "What release to purge the package for")
	purgePkgCmd.Flags().StringVar(&pkgArgs.repositoryName, "repo", "main", "What repository the package is in")
	purgePkgCmd.Flags().StringVar(&pkgArgs.arch, "arch", "", "Limit purging to a single arch only")

	purgeCmd.AddCommand(purgePkgCmd)
}

type packageFilterPredicate func(*repository.RepositoryPackage) bool

func fastlyPurgePkgCmd(cmd *cobra.Command, args []string) {
	alpineReleases, err := releases.Fetch()

	if err != nil {
		utils.Fatalf("Error fetching releases: %s\n", err)
	}

	release := alpineReleases.GetRelBranch(pkgArgs.releaseName)
	if release == nil {
		utils.Fatalf("Error: unknown release %s\n", pkgArgs.releaseName)
	}

	repositories := []*repository.RepositoryWithIndex{}

	var arches []string
	if pkgArgs.arch != "" {
		if !utils.ListContains(release.Arches, pkgArgs.arch) {
			utils.Fatalf("Unknown arch %s\n", pkgArgs.arch)
		}
		arches = []string{pkgArgs.arch}
	} else {
		arches = release.Arches
	}

	for _, arch := range arches {
		repo := repository.NewRepositoryFromComponents(mirror, pkgArgs.releaseName, pkgArgs.repositoryName, arch)
		log.Printf("Downloading index for %s/%s/%s", pkgArgs.releaseName, pkgArgs.repositoryName, arch)
		indexFile, err := utils.FetchFile(repo.IndexUri())

		if err != nil {
			log.Printf("ERROR: fetching index for %s: %s", arch, err)
			continue
		}

		index, err := repository.IndexFromArchive(indexFile.Stream)

		if err != nil {
			log.Printf("ERROR: parsing index for %s: %s", arch, err)
			continue
		}

		repositories = append(repositories, repo.WithIndex(index))
	}

	packages := []*repository.RepositoryPackage{}
	selectedPackages := map[string]bool{}
	for _, pkg := range args {
		selectedPackages[pkg] = true
	}

	var predicate packageFilterPredicate

	switch {
	case pkgArgs.origin:
		predicate = func(pkg *repository.RepositoryPackage) bool {
			return selectedPackages[pkg.Origin]
		}
	case pkgArgs.filename:
		predicate = func(pkg *repository.RepositoryPackage) bool {
			return selectedPackages[pkg.Filename()]
		}
	default:
		predicate = func(pkg *repository.RepositoryPackage) bool {
			return selectedPackages[pkg.Name]
		}
	}

	for _, reposistory := range repositories {
		for _, pkg := range reposistory.Packages() {
			if predicate(pkg) {
				packages = append(packages, pkg)
			}
		}
	}

	if len(packages) == 0 {
		utils.Fatalf("No packages found, nothing to do\n")
	}

	if pkgArgs.dryRun {
		for _, pkg := range packages {
			log.Printf("Would purge %s/%s", pkg.Repository().RepoAbbr(), pkg.Filename())
		}
	} else {
		Purge(packages, 2)
	}
}
