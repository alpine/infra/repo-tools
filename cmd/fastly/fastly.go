package fastly

import (
	"fmt"
	"log"
	"net/http"
	"sync"

	"github.com/spf13/cobra"
	"gitlab.alpinelinux.org/alpine/go/repository"
	"gitlab.alpinelinux.org/alpine/infra/repo-tools/cmd"
)

const (
	mirror = "https://dl-cdn.alpinelinux.org/alpine"
)

var (
	fastlyCmd *cobra.Command
	purgeCmd  *cobra.Command
)

func init() {
	fastlyCmd = &cobra.Command{
		Use:   "fastly",
		Short: "Operations that are specific to fastly (dl-cdn)",
	}
	cmd.AddSubcommand(fastlyCmd)

	purgeCmd = &cobra.Command{
		Use:   "purge",
		Short: "Commands to purge packages",
	}
	fastlyCmd.AddCommand(purgeCmd)
}

func Purge(packages []*repository.RepositoryPackage, threads int) error {
	if threads < 1 {
		return fmt.Errorf("argument 'threads' must be >= 1")
	}

	ch := make(chan *repository.RepositoryPackage, 100)
	wg := &sync.WaitGroup{}

	for i := 0; i < threads; i++ {
		wg.Add(1)
		go purgePackages(ch, wg)
	}

	for _, pkg := range packages {
		ch <- pkg
	}

	close(ch)
	wg.Wait()

	return nil
}

func purgePackages(ch chan *repository.RepositoryPackage, wg *sync.WaitGroup) {
	defer wg.Done()
	client := &http.Client{}

	for pkg := range ch {
		pkgUrl := pkg.Url()
		func() {
			req, err := http.NewRequest("PURGE", pkgUrl, nil)

			if err != nil {
				log.Printf("ERR: Purging package %s: %s\n", pkg.Name, err)
				return
			}

			resp, err := client.Do(req)

			if err != nil {
				log.Printf("ERR: %s\n", err)
				return
			}

			defer resp.Body.Close()

			if resp.StatusCode != 200 {
				log.Printf("ERR: PURGE %s returns %d\n", pkgUrl, resp.StatusCode)
			} else {
				log.Printf("Purged %s/%s", pkg.Repository().RepoAbbr(), pkg.Filename())
			}
		}()
	}
}
