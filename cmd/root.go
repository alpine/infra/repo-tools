package cmd

import (
	"github.com/spf13/cobra"
)

var (
	rootCmd = &cobra.Command{
		Use:   "repo-tools",
		Short: "tools to interact with online Alpine Linux mirrors",
	}
)

func Execute() error {
	return rootCmd.Execute()
}

func AddSubcommand(cmd ...*cobra.Command) {
	rootCmd.AddCommand(cmd...)
}
